Doe de volgende stappen:

    pbcopy < ~/.ssh/id_rsa.pub # MacOS
    
- voeg je ssh public key toe aan je gitlab account
- kloon de volgende projecten:

        git clone git@gitlab.com:eohn/cdac.git
        git clone git@gitlab.com:eohn/cdac_compose.git
        cd cdac_compose/
        docker-compose up
    
- de Jenkins server draait nu op **localhost:8080**, em SonarQube op **localhost:9000**

Als je nu inlogt op Jenkins word je gevraagd om een tijdelijk admin password. Dit haal je als volgt op:

    docker exec -it $(docker ps -a | grep jenkins | cut -d " " -f1) bash

je zit nu in een shell op de Jenkins container

    cat /var/lib/jenkins/secrets/initialAdminPassword
    sudo su -s /bin/bash jenkins
    ssh-keygen
    cat ~/.ssh/id_rsa.pub
    # kopieer deze van je command line. en voeg toe aan user:jenkins-eohn password:aAQ-omG-U8Z-MgP
    git ls-remote -h git@gitlab.com/eohn/cdac.git
    # antwoord 'yes'

    exit
    
je krijgt nu een keuzemenu voor plugins te zien. Kies "Select plugins to install"
Plugins die aan moeten staan: 
- Dashboard View
- Junit Plugin
- Git Plugin
- GitLab Plugin

Prompt "Create First Admin User" 

Vul in en druk op "Save and Finish"

Ga naar **Manage Jenkins > Configure System > GitLab** en vul in:

- Domain: Global
- Kind: GitLab API token
- Scope: Global
- API token: Yq_Xz6ABysx8hiNNyNip


Ga naar **New Item > Pipeline** en maak **cdac-pipeline**. Vul de volgende velden in:

- Pipeline/Definition: Pipeline script from SCM
- Pipeline/Definition/SCM: Git 
- Pipeline/Definition/SCM/Repositories/Repository URL: git@gitlab.com:eohn/cdac.git


            