/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.cd;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author JP
 */
public class HelloTest {

    private static final Properties UIT_PROPERTIES = new Properties();
    private static final Logger LOG = Logger.getLogger(HelloTest.class.getName());
    private static final String UIT_PROPERTIES_FILE = "/uit.properties";
    private static final String REST_SERVICE_URL_KEY = "uit.rest.service.url";
    private final HelloWorldRestServiceClient client = new HelloWorldRestServiceClient(
            UIT_PROPERTIES.getProperty(REST_SERVICE_URL_KEY));

    static {
        try {
            UIT_PROPERTIES.load(HelloTest.class.getResourceAsStream(UIT_PROPERTIES_FILE));
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void testSayHello() {
        assertEquals("Hello Worlds!",client.sayHello());
    }

}
