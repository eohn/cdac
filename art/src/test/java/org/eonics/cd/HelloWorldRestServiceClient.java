/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.cd;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author JP
 */
public class HelloWorldRestServiceClient {

    private static final Logger LOG = Logger.getLogger(HelloWorldRestServiceClient.class.getName());

    private final String baseUrl;
    private final Client client;

    public HelloWorldRestServiceClient(String baseUrl) {
        this.baseUrl = baseUrl;

        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        client = Client.create(clientConfig);
    }

    private WebResource getWebResource(String postfix) {
        return client
                .resource(baseUrl + "/" + postfix);
    }

    public String sayHello() {
        return getWebResource("hello").accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).get(String.class);
    }
}
