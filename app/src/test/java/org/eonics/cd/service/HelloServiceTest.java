/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.cd.service;

import org.eonics.cd.service.HelloService;
import java.net.URISyntaxException;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author Johan
 */
public class HelloServiceTest {

    public HelloServiceTest() {
    }

    @Test
    public void SayHalloTest() throws URISyntaxException {
        Assert.assertEquals("Hello Worlds!", HelloService.sayHello());
    }
}
