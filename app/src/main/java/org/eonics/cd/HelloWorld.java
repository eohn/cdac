/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eonics.cd;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eonics.cd.service.HelloService;
import static spark.Spark.get;

/**
 *
 * @author Johan
 */
public class HelloWorld {
    private static final Logger LOGGER = Logger.getLogger(HelloWorld.class.getName());
    
    private HelloWorld() {}

    public static void main(String[] args) {
        get("/hello", (req, res) -> {
            LOGGER.info("/hello triggered");
            LOGGER.log(Level.FINEST, "Request: {0}", req);
            LOGGER.log(Level.FINEST, "Response: {0}", res);
            
            return HelloService.sayHello();
        });
    }

}
